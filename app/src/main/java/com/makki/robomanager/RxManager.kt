package com.makki.robomanager

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


/**
 * @author Max.Popovych on 10.05.18.
 */
object RxManager {
    fun getDelayResult(delay: Long): Observable<String> {
        return Observable.just("test").subscribeOn(Schedulers.io()).delay(delay, TimeUnit.MILLISECONDS)
    }
}
