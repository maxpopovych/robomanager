package com.makki.robomanager.activities.views

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.ViewTreeObserver
import com.makki.robomanager.R


/**
 * @author Max.Popovych on 09.05.18.
 *
 * Custom rotating View for showing that an operation is in progress
 */
class SpinnerView(context: Context, attrs: AttributeSet) : View(context, attrs), ValueAnimator.AnimatorUpdateListener, ViewTreeObserver.OnGlobalLayoutListener {
    private var radius = 5f
    private var color: Int = Color.BLACK
    private val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var rects: Array<RectF> = emptyArray()

    private val rotateAnimation = ObjectAnimator.ofFloat(this, "rotation", 0f, 360f)
            .also {
                it.duration = 1500
                it.repeatMode = ValueAnimator.RESTART
                it.repeatCount = ValueAnimator.INFINITE
            }

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.SpinnerView, 0, 0)
        color = a.getColor(R.styleable.SpinnerView_color, color)
        radius = a.getDimension(R.styleable.SpinnerView_radius, radius)
        a.recycle()

        paint.color = color

        viewTreeObserver.addOnGlobalLayoutListener (this)
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)

        if (visibility == View.VISIBLE) {
            rotateAnimation.start()
            return
        }
        rotateAnimation.pause()
    }

    override fun onGlobalLayout() {
        val middleW = (width / 2).toFloat()
        val middleH = (height / 2).toFloat()
        rects = arrayOf(RectF(middleW - radius, 0f, middleW + radius, middleH),
                RectF(middleW, middleH - radius, width.toFloat(), middleH + radius),
                RectF(middleW - radius, middleH, middleW + radius, height.toFloat()),
                RectF(0f, middleH - radius, middleW, middleH + radius))
    }

    override fun onAnimationUpdate(animation: ValueAnimator?) {
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (rects.isEmpty()) {
            onGlobalLayout()
        }

        canvas.save()

        for (rect in rects) {
            canvas.drawOval(rect, paint)
        }
        canvas.restore()
    }
}
