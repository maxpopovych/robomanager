package com.makki.robomanager.activities.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.makki.robomanager.R
import com.makki.robomanager.model.DeviceAsset

/**
 * @author Max.Popovych on 01.06.18.
 */
class DeviceAdapter(private var list: List<DeviceAsset>) : RecyclerView.Adapter<DeviceViewHolder>() {

    interface DeviceItemCallback {
        fun onClick(asset: DeviceAsset?)
    }

    private var callback: DeviceItemCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_device_element, parent, false)
        return DeviceViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: DeviceViewHolder?, position: Int) {
        holder?.onBindViewHolder(list[position], callback)
    }

    fun setCallback(callback: DeviceItemCallback) {
        this.callback = callback
    }

    fun setItems(list: List<DeviceAsset>) {
        this.list = list
        notifyDataSetChanged()
    }
}

class DeviceViewHolder(val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
    var asset: DeviceAsset? = null

    var callback: DeviceAdapter.DeviceItemCallback? = null

    @SuppressLint("SetTextI18n")
    fun onBindViewHolder(deviceAsset: DeviceAsset, callback: DeviceAdapter.DeviceItemCallback?) {
        asset = deviceAsset
        view.findViewById<TextView>(R.id.device_title_view).text = deviceAsset.name
        view.findViewById<TextView>(R.id.device_descr_view).text = "Active task: " + deviceAsset.activeTaskId

        this.callback = callback
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        callback?.onClick(asset)
    }

}