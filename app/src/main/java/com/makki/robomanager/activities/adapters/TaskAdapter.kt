package com.makki.robomanager.activities.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.makki.robomanager.R
import com.makki.robomanager.model.TaskAsset

/**
 * @author Max.Popovych on 01.06.18.
 */
class TaskAdapter(private var list: List<TaskAsset>) : RecyclerView.Adapter<TaskViewHolder>() {

    interface TaskItemCallback {
        fun onClick(task: TaskAsset?)
    }

    private var callback: TaskItemCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_task_element, parent, false)
        view.setOnClickListener {  }
        return TaskViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: TaskViewHolder?, position: Int) {
        holder?.onBindViewHolder(list[position], callback)
    }

    fun setCallback(callback: TaskItemCallback) {
        this.callback = callback
    }

    fun setItems(list: List<TaskAsset>) {
        this.list = list
        notifyDataSetChanged()
    }

}

class TaskViewHolder(val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
    var asset: TaskAsset? = null

    var callback: TaskAdapter.TaskItemCallback? = null

    @SuppressLint("SetTextI18n")
    fun onBindViewHolder(taskAsset: TaskAsset, callback: TaskAdapter.TaskItemCallback?) {
        view.findViewById<TextView>(R.id.task_title_view).text = taskAsset.name

        this.callback = callback
        asset = taskAsset
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        callback?.onClick(asset)
    }

}