package com.makki.robomanager.activities.views

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import com.makki.robomanager.R

/**
 * @author Max.Popovych on 23.06.18.
 */
class CircleColorView(context: Context, attrs: AttributeSet?) : View(context, attrs) {

    companion object {
        public const val NotAvailable = 0
        public const val StandBy = 1
        public const val Running = 2
    }

    private var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var colorNA = context.resources.getColor(R.color.colorAccent)
    private var colorStandby = context.resources.getColor(R.color.colorStandBy)
    private var colorRunning = context.resources.getColor(R.color.colorRunning)

    private val scaleAnimation = ObjectAnimator.ofFloat(this, "scaleX", "scaleY", Path().also {
        it.moveTo(0.7f, 0.7f)
        it.lineTo(1.4f, 1.4f)
    })
            .also {
                it.duration = 1500
                it.repeatMode = ValueAnimator.REVERSE
                it.repeatCount = ValueAnimator.INFINITE
            }

    private var color: Int = context.resources.getColor(R.color.colorAccent)
        set(value) {
            field = value
            paint.color = value
            invalidate()
        }

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CircleColorView, 0, 0)
        color = a.getColor(R.styleable.CircleColorView_circleColor, color)
        a.recycle()
    }

    public fun setState(state: Int) {
        when (state) {
            NotAvailable -> color = colorNA
            StandBy -> color = colorStandby
            Running -> color = colorRunning
        }
        if (state == Running) {
            scaleAnimation.start()
        } else {
            scaleAnimation.pause()
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.save()

        canvas.drawOval(0f, 0f, canvas.width.toFloat(), canvas.height.toFloat(), paint)
    }
}