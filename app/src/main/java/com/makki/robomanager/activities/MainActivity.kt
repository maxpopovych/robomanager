package com.makki.robomanager.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.makki.robomanager.R
import com.makki.robomanager.fragments.LoginFragment
import java.util.concurrent.TimeUnit

/**
 * @author Max.Popovych on 05.03.18.
 */
class MainActivity : AppCompatActivity() {

    private var backTimeStamp = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(2)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            supportFragmentManager.findFragmentByTag("CONTAINER")
        } else {
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, LoginFragment.newInstance(), "CONTAINER")
                    //                .add(R.id.container, NavigationFragment.newInstance())
                    .commit()
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size > 1) return super.onBackPressed()

        val curTime = System.currentTimeMillis()
        if (curTime < backTimeStamp) {
            return super.onBackPressed()
        }
        backTimeStamp = curTime + TimeUnit.SECONDS.toMillis(2)
        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show()
    }

}