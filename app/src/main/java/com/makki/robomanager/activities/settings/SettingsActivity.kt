package com.makki.robomanager.activities.settings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.makki.robomanager.R

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
    }
}
