package com.makki.robomanager.managers

import android.net.Uri
import android.util.Log
import com.makki.robomanager.managers.socket.SocketManager
import com.makki.robomanager.managers.utils.ExecuteTask
import com.makki.robomanager.managers.utils.JsonParser
import com.makki.robomanager.managers.utils.LoginResponse
import com.makki.robomanager.model.DeviceAsset
import com.makki.robomanager.model.TaskAsset
import com.makki.robomanager.model.events.SocketResponse
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * @author Max.Popovych on 18.06.18.
 *
 * A singleton manager which is a main access point for REST requests.
 * Is wrapped around by DataManager
 */
object RequestManager {

    private const val hostUrl = "http://vps558496.ovh.net"
    private const val loginUrl = "api/login"
    private const val tasksUrl = "api/task/list"
    private const val deviceUrl = "api/device/list"
    private const val executeUrl = "api/device"
    private const val apiKeyConst = "api_token"

    private val debugMode = false

    private var apiKey : String = ""

    private val socketManager = SocketManager
    private val stateTracker = BehaviorSubject.create<String>()
    private var user: String = ""

    private val client = OkHttpClient()

    /**
     * @property login name of the user.
     * @property pass password of the user.
     * @return An rx maybe object with a response object LoginResponse.
     */
    fun login(login: String, pass: String) = Maybe.create<LoginResponse> { e ->

        if (debugMode) {
            val response = LoginResponse(StubbedJson.jsonLogin)
            if (response.isSuccessful) {
                apiKey = response.getApiToken
            }
            e.onSuccess(response)
            Log.e("RequestManager", "Login successful -> $login")
            return@create
        }

        val req = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("login", login)
                .addFormDataPart("password", pass)
                    .build()

        try {
            val url = Uri.parse(hostUrl).buildUpon()
                    .appendPath(loginUrl)

            Log.e("RequestManager", "Request url -> $url")

            val res = client.newCall(Request.Builder()
                    .url(url.toString())
                    .post(req)
                    .build()).execute()

            if (res.isSuccessful) {
                val response = LoginResponse(res.body()?.string()?: "")
                if (response.isSuccessful) {
                    apiKey = response.getApiToken
                    Log.e("RequestManager", "Login successful -> $login")
                    e.onSuccess(response)
                    return@create
                }
                Log.e("RequestManager", "Login not successful -> $login \n error -> ${response.getErrorMsg}")
            } else {
                Log.e("RequestManager", res.body()?.string()?: "")
                Log.e("RequestManager", res.message())
            }

        } catch (ex: IOException) {
            ex.printStackTrace()
        } finally { e.onComplete() }
    }.delay(if (debugMode) 5000 else 0, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io())

    /**
     * Gets a list of devices available for the user.
     * @return An rx observable with devices
     */
    fun getDeviceList() = Observable.create<DeviceAsset> { e ->
        if (debugMode) {
            JsonParser.getDevices(StubbedJson.jsonDeviceList).getDevices().forEach { e.onNext(it) }
            e.onComplete()
            return@create
        }

        try {
            val url = Uri.parse(hostUrl).buildUpon()
                    .appendPath(deviceUrl)
                    .appendQueryParameter(apiKeyConst, apiKey)

            Log.e("RequestManager", "Request url -> $url")

            val res = client.newCall(Request.Builder()
                    .url(url.toString())
                    .get()
                    .build()).execute()

            if (res.isSuccessful) {
                JsonParser.getDevices(res.body()?.string()?: "").getDevices().forEach { e.onNext(it) }
            }

        } catch (ex: IOException) {
            ex.printStackTrace()
        } finally { e.onComplete() }
    }.subscribeOn(Schedulers.io())

    /**
     * Gets a list of tasks available for the user.
     * @return An rx observable with tasks
     */
    fun getTaskList() = Observable.create<TaskAsset> { e ->
        if (debugMode) {
            JsonParser.getTasks(StubbedJson.jsonTaskList).getTasks().forEach { e.onNext(it) }
            e.onComplete()
            return@create
        }

        try {
            val url = Uri.parse(hostUrl).buildUpon()
                    .appendPath(tasksUrl)
                    .appendQueryParameter(apiKeyConst, apiKey)

            Log.e("RequestManager", "Request url -> $url")

            val res = client.newCall(Request.Builder()
                    .url(url.toString())
                    .get()
                    .build()).execute()

            if (res.isSuccessful) {
                JsonParser.getTasks(res.body()?.string()?: "").getTasks().forEach { e.onNext(it) }
            }

        } catch (ex: IOException) {
            ex.printStackTrace()
        } finally { e.onComplete() }
    }.subscribeOn(Schedulers.io())

    /**
     * Executes a task on the device
     * @property task the asset of the task which needs to be executed
     * @property device the asset of the device for which the task needs to be executed
     * @return An rx single with a boolean of success
     */
    fun executeTask(task: TaskAsset, device: DeviceAsset) = Single.create<Boolean> { e ->
        try {
            val url = Uri.parse(hostUrl).buildUpon()
                    .appendPath(executeUrl)
                    .appendPath(device.id.toString())
                    .appendPath("action")
                    .appendQueryParameter(apiKeyConst, apiKey)
                    .appendQueryParameter("id_task", task.id.toString())
                    .appendQueryParameter("action", "start")

            Log.e("RequestManager", "Request url -> $url")

            val req = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("", "")
                    .build()

            val res = client.newCall(Request.Builder()
                    .url(url.toString())
                    .post(req)
                    .build()).execute()

            if (res.isSuccessful) {
                e.onSuccess(ExecuteTask(res.body()?.string()?: "").isSuccessful)
            }
        } catch (ex: IOException) {
            e.onSuccess(false)
            ex.printStackTrace()
        } finally {
            e.onSuccess(false)
        }
    }.subscribeOn(Schedulers.io())

    /**
     * Cancels all of the current tasks on the selected device
     * @property device the asset of the device for which the state needs to be cancels
     * @return An rx single with a boolean of success
     */
    fun stopTask(device: DeviceAsset) = Single.create<Boolean> { e ->
        try {
            val url = Uri.parse(hostUrl).buildUpon()
                    .appendPath(executeUrl)
                    .appendPath(device.id.toString())
                    .appendPath("action")
                    .appendQueryParameter(apiKeyConst, apiKey)
                    .appendQueryParameter("action", "stop")

            Log.e("RequestManager", "Request url -> $url")

            val req = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("", "")
                    .build()

            val res = client.newCall(Request.Builder()
                    .url(url.toString())
                    .post(req)
                    .build()).execute()

            if (res.isSuccessful) {
                e.onSuccess(ExecuteTask(res.body()?.string()?: "").isSuccessful)
            }
        } catch (ex: IOException) {
            e.onSuccess(false)
            ex.printStackTrace()
        } finally {
            e.onSuccess(false)
        }
    }.subscribeOn(Schedulers.io())

    /**
     * Tracks socket message for a specific device
     * @property device the asset of the device for which tracking is needed
     * @return An rx observable with socket responses, works as a subject
     */
    fun trackState(device: DeviceAsset) : Observable<SocketResponse> {
//        stopTrackingState() // check if needs to be used here

        return socketManager.trackResponses("$hostUrl/ws/status?"
                + "id_user=" + device.userId
                + "&id_device=" + device.id)
    }

    /**
     * Stops current tracking
     */
    fun stopTrackingState() = socketManager.closeConnection()

}