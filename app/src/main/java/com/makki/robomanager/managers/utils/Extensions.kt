package com.makki.robomanager.managers.utils

import android.support.v4.app.FragmentTransaction

/**
 * @author Max.Popovych on 22.06.18.
 */

fun FragmentTransaction.animateTransition(): FragmentTransaction {
    this.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
    this.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
    return this
}