package com.makki.robomanager.managers

/**
 * @author Max.Popovych on 22.06.18.
 *
 * Used in debug mode with no internet connection
 */
object StubbedJson {
    const val jsonLogin =
            "{" +
                    "success: true," +
                    "redirect: \"\"," +
                    "error: []," +
                    "api_token: \"2383gb3hf834f3ugf834\"" +
                    "}"

    const val jsonDeviceList =
            "[" +
                    "{" +
                    "id: 1," +
                    "id_user: 1," +
                    "id_active_task: null," +
                    "created_at: null," +
                    "name: \"Device GOBLINxR3\"" +
                    "}, " +

                    "{" +
                    "id: 2," +
                    "id_user: 1," +
                    "id_active_task: null," +
                    "created_at: null," +
                    "name: \"Device HawkEye\"" +
                    "}" +
                    "]"

    const val jsonTaskList =
            "[" +
                    "{" +
                    "id: 1," +
                    "id_user: 1," +
                    "name: \"START\"," +
                    "task: \"add msg \\\"123123123\\\"\"," +
                    "created_at: null," +
                    "updated_at: null" +
                    "}," +

                    "{" +
                    "id: 3," +
                    "id_user: 1," +
                    "name: \"STOP\"," +
                    "task: \"add msg \\\"123123123\\\"\"," +
                    "created_at: null," +
                    "updated_at: null" +
                    "}" +
                    "]"
}