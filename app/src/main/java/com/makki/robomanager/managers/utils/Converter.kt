package com.makki.robomanager.managers.utils

import com.makki.robomanager.model.events.SocketResponse
import org.json.JSONObject

/**
 * @author Max.Popovych on 31.05.18.
 */

class Converters {
    public val default = DefaultConverter()

    public val error = ErrorResponseConverter()

    private val list = ArrayList<Converter>().also {
        //add custom converters here
        it.add(StatusConverter())
    }

    public fun forEach(obj: JSONObject): SocketResponse {
        for (interceptor in list) {
            if (interceptor.check(obj)) {
                return interceptor.generateResponse(obj)
            }
        }
        return default.generateResponse(obj)
    }
}

abstract class Converter {
    public fun check(obj: JSONObject): Boolean {
        return checkImpl(obj)
    }

    protected abstract fun checkImpl(obj: JSONObject): Boolean

    abstract fun generateResponse(obj: JSONObject): SocketResponse
}

class DefaultConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        return true
    }

    override fun generateResponse(obj: JSONObject): SocketResponse {
        return SocketResponse(false)
    }
}

class ErrorResponseConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        return true
    }

    override fun generateResponse(obj: JSONObject) = SocketResponse.error()

    fun generateResponse(e: Throwable) = SocketResponse.error().also {
        it.throwable = e
    }
}

class StatusConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        return obj.has("task_id") && obj.has("location")
    }

    override fun generateResponse(obj: JSONObject): SocketResponse {
        return SocketResponse(true).also {
            it.content = obj.toString()
        }
    }
}
