package com.makki.robomanager.managers

import android.content.Context
import com.makki.robomanager.App
import com.makki.robomanager.model.DeviceAsset
import com.makki.robomanager.model.TaskAsset

/**
 * @author Max.Popovych on 20.06.18.
 *
 * The main singleton manager for data operation with credentiasl, REST and WebSocket
 * Wraps RequestManager and SocketManager
 */
object DataManager {

    var deviceListCache = ArrayList<DeviceAsset>()
    var taskListCache = ArrayList<TaskAsset>()

    /**
     * Save user credentials localy
     *
     * @property login name of the user.
     * @property pass password of the user.
     */
    fun setCredentials(login: String, pass: String) {
        val sp = App.applicationContext().getSharedPreferences("credentials", Context.MODE_PRIVATE)
        val editor = sp.edit()
        editor.putString("login", login)
        editor.putString("password", pass)
        editor.apply()
    }

    /**
     * Get user credentials from a local storage
     * @return a Pair object with credentials, with login as first
     */
    fun getCredentials(): Pair<String, String> {
        val sp = App.applicationContext().getSharedPreferences("credentials", Context.MODE_PRIVATE)
        val log = sp.getString("login", "")
        val pass = sp.getString("password", "")
        return Pair<String, String>(log, pass)
    }

    /**
     * @property login name of the user.
     * @property pass password of the user.
     * @return An rx maybe object with a response object LoginResponse.
     */
    fun login(login: String, pass: String) = RequestManager.login(login, pass)

    /**
     * Gets a list of devices available for the user.
     * @return An rx observable with devices
     */
    fun getDeviceList() = RequestManager
            .getDeviceList()
            .toList()
            .doAfterSuccess {
                deviceListCache.clear()
                deviceListCache.addAll(it)
            }

    /**
     * Gets a list of tasks available for the user.
     * @return An rx observable with tasks
     */
    fun getTaskList() = RequestManager
            .getTaskList()
            .toList()
            .doAfterSuccess {
                taskListCache.clear()
                taskListCache.addAll(it)
            }

    /**
     * Executes a task on the device
     * @property task the asset of the task which needs to be executed
     * @property device the asset of the device for which the task needs to be executed
     * @return An rx single with a boolean of success
     */
    fun executeTask(task: TaskAsset, device: DeviceAsset) = RequestManager.executeTask(task, device)

    /**
     * Cancels all of the current tasks on the selected device
     * @property device the asset of the device for which the state needs to be cancels
     * @return An rx single with a boolean of success
     */
    fun stopTask(device: DeviceAsset) = RequestManager.stopTask(device)

    /**
     * Tracks socket message for a specific device
     * @property device the asset of the device for which tracking is needed
     * @return An rx observable with socket responses, works as a subject
     */
    fun trackState(device: DeviceAsset) = RequestManager.trackState(device)

    /**
     * Stops current tracking
     */
    fun stopTrackingState() = RequestManager.stopTrackingState()

}