package com.makki.robomanager.managers.utils

import com.makki.robomanager.model.DeviceAsset
import com.makki.robomanager.model.TaskAsset
import com.makki.robomanager.model.events.StatusAsset
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * @author Max.Popovych on 19.06.18.
 */
class JsonParser {
    companion object {
        fun getLogin(json: String) = LoginResponse(json)
        fun getDevices(json: String) = DeviceList(json)
        fun getTasks(json: String) = TaskList(json)
        fun getStatusAsset(json: String): StatusAsset {
            try {
                return JSONObject(json).toStatus()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return StatusAsset()
        }
    }
}

class LoginResponse(msg: String) {
    var isSuccessful: Boolean = false
    var getErrorMsg: String = ""
    var getRedirect: String = ""
    var getApiToken: String = ""

    init {
        try {
            val json: JSONObject = JSONObject(msg)
            isSuccessful = json.optBoolean("success", false)
            getErrorMsg = json.optString("error", "Unknown error from API")
            getRedirect = json.optString("redirect", "")
            getApiToken = json.optString("api_token", "")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}

class DeviceList(msg: String) {
    private val devices = ArrayList<DeviceAsset>()

    init {
        try {
            val json = JSONArray(msg)
            for (i in 0 until json.length()) {
                devices.add(json.getJSONObject(i).toDevice())
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun getDevices() = devices
}

class TaskList(msg: String) {
    private val tasks = ArrayList<TaskAsset>()

    init {
        try {
            val json = JSONArray(msg)
            for (i in 0 until json.length()) {
                tasks.add(json.getJSONObject(i).toTask())
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun getTasks() = tasks
}

class ExecuteTask(msg: String) {
    var isSuccessful: Boolean = false

    init {
        try {
            val json: JSONObject = JSONObject(msg)
            isSuccessful = json.optBoolean("success", false)
                    && !json.optBoolean("error", false)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }
}

fun JSONObject.toDevice(): DeviceAsset {
    return DeviceAsset().also {
        it.id = this.optInt("id", 0)
        it.userId = this.optInt("id_user", 0)
        it.activeTaskId = this.optInt("id_active_task", 0)
        it.name = this.optString("name", "N/A")
        it.createdAt = this.optString("created_at", "")
        it.updatedAt = this.optString("updated_at", "")
    }
}

fun JSONObject.toTask(): TaskAsset {
    return TaskAsset().also {
        it.id = this.optInt("id", 0)
        it.userId = this.optInt("id_user", 0)
        it.name = this.optString("name", "N/A")
        it.task = this.optString("task", "")
        it.createdAt = this.optString("created_at", "")
        it.updatedAt = this.optString("updated_at", "")
    }
}

fun JSONObject.toStatus(): StatusAsset {
    return StatusAsset().also {
        it.taskId = this.optString("task_id", "")
        val obj = this.optJSONObject("location")
        it.lat = obj.optString("lat")
        it.long = obj.optString("lng")
    }
}