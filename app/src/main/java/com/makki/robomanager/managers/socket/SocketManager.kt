package com.makki.robomanager.managers.socket

import android.util.Log
import android.widget.Toast
import com.makki.robomanager.App
import com.makki.robomanager.model.events.SResponseType
import com.makki.robomanager.model.events.SocketResponse
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

/**
 * @author Max.Popovych on 06.03.18.
 *
 * A singleton object for managing WebSocket connection and messages
 */
object SocketManager : Consumer<SocketResponse> {

    private const val TAG: String = "SocketManager"
    private var host: String = "ws://robotomanager.local:8083/ws/"

    private val events: Subject<SocketResponse> = BehaviorSubject.create()
    private val wrapper: SocketSubject = SocketSubject(events)
    private var socket: SocketWrapper? = null
    private var connectionJob = initTask()

    private var loginName: String = "Guest"

    /**
     * Rx specific method to intercept all of the responses from the event subject
     */
    override fun accept(response: SocketResponse) {
        if (response.type == SResponseType.ClosedMessage) {
            closeConnection()
        }

        if (response.type == SResponseType.FailureMessage) {
            onError()
            closeConnection()
        }
    }

    /**
     * Tracks responses for the selected Url of WebSocket
     * Also needs to be called to set a host url
     * @return rx subject which will return SocketResponses
     */
    public fun trackResponses(url: String): Observable<SocketResponse> {
        host = url
        reconnectSocket()

        return events.doOnNext(this)
                .subscribeOn(Schedulers.io())!!
    }

    private fun initTask() = Completable
            .fromAction { reconnectSocket() }
            .cache()

    /**
     * Reconnects to the socket
     */
    private fun reconnectSocket() {
        Log.w(TAG, "Trying to reconnect")

        Log.e("SocketManager", "Attempted url -> \n $host")

        socket = SocketBuilder()
                .loginAs(loginName)
                .connectTo(host)
                .respondTo(wrapper)
                .timeOutIn(5000)
                .build()
    }

    /**
     * Closes connection and sets the job for socket connection to the inial state
     */
    fun closeConnection() {
        socket?.getSocket()?.cancel()
        connectionJob = initTask()
    }

    private fun onError() {
        App.getMainHandler()?.post {
            Toast.makeText(App.applicationContext(), "Connection got interrupted", Toast.LENGTH_LONG).show()
        }
    }

}