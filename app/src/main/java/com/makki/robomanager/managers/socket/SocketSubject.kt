package com.makki.robomanager.managers.socket

import android.util.Log
import com.makki.robomanager.managers.utils.Converters
import com.makki.robomanager.model.events.SResponseType
import com.makki.robomanager.model.events.SocketResponse
import io.reactivex.subjects.Subject
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString
import org.json.JSONObject
import java.nio.charset.Charset

/**
 * @author Max.Popovych on 13.05.18.
 * Wrapper around a subject to capture all of the messages coming from the WebSocket
 * Uses a Converter class to turn Json's into SocketResponse
 */
class SocketSubject(private val subject: Subject<SocketResponse>) : WebSocketListener() {

    companion object {
        private const val TAG: String = "SocketSubject"
    }

    private val converters = Converters()

    override fun onOpen(webSocket: WebSocket, response: Response) {
        Log.e("SocketManager", "Opened connection -> ${response.body()?.string()}")
        val event = SocketResponse(false)
                .also {
                    it.type = SResponseType.OpenMessage
                }
        subject.onNext(event)
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        Log.e("SocketManager", "Incoming message -> $text")
        val obj = JSONObject(text)

        subject.onNext(converters.forEach(obj).also {
            it.type = SResponseType.StringMessage
        })
    }

    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
        val obj = JSONObject(bytes.string(Charset.defaultCharset()))
        Log.e("SocketManager", "Incoming message -> $obj")

        subject.onNext(converters.forEach(obj).also {
            it.type = SResponseType.ByteMessage
        })
    }

    override fun onClosing(webSocket: WebSocket?, code: Int, reason: String) {
        Log.e("SocketManager", "Closing connection, reason -> $reason")
        val event = SocketResponse(false)
                .also {
                    it.type = SResponseType.ClosingMessage
                }
        subject.onNext(event)
    }

    override fun onClosed(webSocket: WebSocket?, code: Int, reason: String) {
        Log.e("SocketManager", "Closed connection, reason -> $reason")
        val event = SocketResponse(false)
                .also {
                    it.type = SResponseType.ClosedMessage
                }
        subject.onNext(event)
    }

    override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
        if (t != null) {
            Log.e(TAG, "Encountered error in socketManager", t)
            subject.onNext(converters.error.generateResponse(t))
        }
    }
}
