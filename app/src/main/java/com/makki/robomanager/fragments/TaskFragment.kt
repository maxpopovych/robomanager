package com.makki.robomanager.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import com.makki.robomanager.App
import com.makki.robomanager.R
import com.makki.robomanager.activities.adapters.TaskAdapter
import com.makki.robomanager.activities.views.CircleColorView
import com.makki.robomanager.managers.DataManager
import com.makki.robomanager.model.DeviceAsset
import com.makki.robomanager.model.TaskAsset
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.fragment_task.*
import java.util.concurrent.TimeUnit

/**
 * @author Max.Popovych on 22.06.18.
 */
class TaskFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, TaskAdapter.TaskItemCallback, View.OnLongClickListener {
    companion object {
        fun newInstance(asset: DeviceAsset?) = TaskFragment().also {
            if (asset == null) return@also
            it.arguments = Bundle().also {
                it.putInt("id", asset.id)
                it.putParcelable("item", asset)
            }
        }
    }

    private lateinit var deviceName: TextView
    private lateinit var deviceStatus: TextView
    private lateinit var circleStatus: CircleColorView
    private lateinit var deviceTimestamp: TextView
    private lateinit var circleTimestamp: CircleColorView
    private lateinit var taskList: RecyclerView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var asset: DeviceAsset
    private var disposableExecute = Disposables.disposed()
    private var disposableAssets = Disposables.disposed()
    private var disposableTrack = Disposables.disposed()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        asset = arguments?.getParcelable("item") ?: DeviceAsset()
        return inflater.inflate(R.layout.fragment_task, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        taskList = task_list_view
        taskList.layoutManager = LinearLayoutManager(context)
        taskList.animation = AnimationUtils.loadAnimation(context, R.anim.abc_slide_in_bottom)

        deviceName = device_name
        deviceStatus = device_status
        circleStatus = circle_status
        deviceTimestamp = device_timestamp
        circleTimestamp = circle_timeStamp
        abort_button.setOnLongClickListener(this)


        track.setOnClickListener(this)

        if (asset.isValid()) {
            deviceName.text = asset.name
            if (!asset.createdAt.isBlank() && asset.createdAt != "null") {

                deviceTimestamp.text = asset.createdAt
            }

            DataManager.getTaskList()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { list ->
                        val search = list.find { it.id.toString() == asset.activeTaskId.toString() }
                        if (search != null && !search.name.isBlank()) {
                            deviceStatus.text = "Status: last task '${search.name}'"
                            deviceStatus.setTextColor(context.resources.getColor(R.color.colorStandBy))
                            circleStatus.setState(CircleColorView.StandBy)
                        }
                    }
        }

        refreshLayout = refresh_layout
        refreshLayout.setOnRefreshListener(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        disposableAssets.dispose()
        disposableAssets = DataManager.getTaskList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    taskList.adapter = TaskAdapter(list).also {
                        it.setCallback(this)
                    }
                }
    }

    override fun onDetach() {
        super.onDetach()

        disposableExecute.dispose()
        disposableAssets.dispose()
        disposableTrack.dispose()

        DataManager.stopTrackingState()
    }

    override fun onRefresh() {
        disposableAssets.dispose()
        disposableAssets = DataManager.getTaskList()
                .timeout(15000, TimeUnit.MILLISECONDS, { obs ->
                    refreshLayout.isRefreshing = false
                    App.getMainHandler()?.post {
                        Toast.makeText(context, "Failed to fetch", Toast.LENGTH_SHORT).show()
                    }
                    obs.onSubscribe(Disposables.disposed())
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    (taskList.adapter as TaskAdapter).setItems(list)
                    refreshLayout.isRefreshing = false
                }
    }

    override fun onClick(v: View?) {
        disposableTrack.dispose()
        disposableTrack = DataManager.trackState(asset)
                .observeOn(AndroidSchedulers.mainThread())
                .filter { it.isMessage() }
                .subscribe { response ->
                    if (response.toAsset().taskId.toIntOrNull() != null && response.toAsset().taskId.toInt() != 0) {
                        val search = DataManager.taskListCache.find { it.id.toString() == response.toAsset().taskId }
                        if (search != null && !search.name.isBlank()) {
                            deviceStatus.text = "Status: executing task '${search.name}'"
                            deviceStatus.setTextColor(App.getRes().getColor(R.color.colorRunning))
                            circleStatus.setState(CircleColorView.Running)
                            Toast.makeText(context, "Updated state: ${search.name}", Toast.LENGTH_SHORT).show()
                            return@subscribe
                        }
                    }
                    deviceStatus.text = "Status: N/A"
                    deviceStatus.setTextColor(context.resources.getColor(R.color.colorAccent))
                    circleStatus.setState(CircleColorView.NotAvailable)
                    Toast.makeText(context, "Updated state: Not executing", Toast.LENGTH_SHORT).show()
                }
    }

    override fun onClick(task: TaskAsset?) {
        if (task == null) return

        disposableExecute.dispose()
        disposableExecute = DataManager.executeTask(task, asset)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result ->
                    if (!result) {
                        deviceStatus.text = "Status: Failed to execute task '${task.name}'"
                        deviceStatus.setTextColor(context.resources.getColor(R.color.colorAccent))
                        circleStatus.setState(CircleColorView.NotAvailable)
                        return@subscribe
                    }
                    deviceStatus.text = "Status: executing task '${task.name}'"
                    deviceStatus.setTextColor(context.resources.getColor(R.color.colorRunning))
                    circleStatus.setState(CircleColorView.Running)
                }
    }

    override fun onLongClick(v: View?): Boolean {
        disposableExecute.dispose()
        disposableExecute = DataManager.stopTask(asset)
                .observeOn(AndroidSchedulers.mainThread())
                .filter { it }
                .subscribe {
                    deviceStatus.text = "Status: N/A"
                    deviceStatus.setTextColor(context.resources.getColor(R.color.colorAccent))
                    circleStatus.setState(CircleColorView.NotAvailable)
                }
        return true
    }


}