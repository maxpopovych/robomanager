package com.makki.robomanager.fragments


import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.makki.robomanager.App
import com.makki.robomanager.BuildConfig
import com.makki.robomanager.R
import com.makki.robomanager.activities.views.SpinnerView
import com.makki.robomanager.managers.DataManager
import com.makki.robomanager.managers.utils.animateTransition
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.concurrent.TimeUnit


class LoginFragment : Fragment(), TextWatcher, View.OnClickListener {
    companion object {
        fun newInstance() = LoginFragment()
    }

    lateinit var nameView: EditText
    lateinit var passView: EditText

    lateinit var remindPassView: TextView
    lateinit var registerView: TextView

    lateinit var loginLayout: ViewGroup
    lateinit var spinner: SpinnerView

    private var disposable: Disposable = Disposables.disposed()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nameView = view.findViewById(R.id.login_input_view)
        passView = view.findViewById(R.id.pass_input_view)
        registerView = view.findViewById(R.id.register_view)
        remindPassView = view.findViewById(R.id.remind_pass_view)
        loginLayout = view.findViewById(R.id.login_layout)
        spinner = view.findViewById(R.id.spinner)

        view.viewTreeObserver.addOnGlobalLayoutListener({
            val r = Rect()
            view.getWindowVisibleDisplayFrame(r)
            val heightDiff = view.rootView.height - (r.bottom - r.top)

            if (heightDiff > 200) {
                remindPassView.visibility = View.GONE
                registerView.visibility = View.GONE

            } else {
                remindPassView.visibility = View.VISIBLE
                registerView.visibility = View.VISIBLE
            }
        })

        login_button.setOnClickListener(this)

        nameView.addTextChangedListener(this)
        passView.addTextChangedListener(this)

        val cred = DataManager.getCredentials()
        if (!cred.first.isBlank() && !cred.second.isBlank()) {
            nameView.setText(cred.first)
            passView.setText(cred.second)
        }
    }

    override fun afterTextChanged(s: Editable?) {
        login_button.isEnabled = nameView.text.isNotBlank() && passView.text.isNotBlank()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

    override fun onDetach() {
        super.onDetach()

        disposable.dispose()
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.login_button) tryLogin()
    }

    private fun showSpinner() {
        spinner.visibility = View.VISIBLE
        loginLayout.alpha = 0.5f
    }

    private fun hideSpinner() {
        spinner.visibility = View.GONE
        loginLayout.alpha = 1f
    }

    private fun tryLogin() {
        hideKeyboard()
        val login = nameView.text.toString()
        val pass = passView.text.toString()

        disposable.dispose()
        disposable = DataManager.login(login, pass)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showSpinner() }
                .doOnComplete { hideSpinner() }
                .timeout(15000, TimeUnit.MILLISECONDS, {
                    App.getMainHandler()?.post {
                        Toast.makeText(context, "Connection timeout", Toast.LENGTH_SHORT).show()
                    }
                    it.onComplete()
                })
                .switchIfEmpty { showFailToast() }
                .subscribe {
                    if (it.isSuccessful) {
                        DataManager.setCredentials(login, pass)

                        fragmentManager.beginTransaction()
                                .animateTransition()
                                .add(id, DeviceFragment.newInstance())
                                .commit()
                    } else {
                        if (!BuildConfig.DEBUG) DataManager.setCredentials("", "")
                        showFailToast()
                    }
        }
    }

    private fun showFailToast() = App.getMainHandler()?.post {
        Toast.makeText(context, "Failed to login", Toast.LENGTH_SHORT).show()
    }

    private fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}
