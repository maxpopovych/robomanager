package com.makki.robomanager.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.makki.robomanager.App
import com.makki.robomanager.R
import com.makki.robomanager.activities.adapters.DeviceAdapter
import com.makki.robomanager.managers.DataManager
import com.makki.robomanager.managers.utils.animateTransition
import com.makki.robomanager.model.DeviceAsset
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import java.util.concurrent.TimeUnit

class DeviceFragment : Fragment(), DeviceAdapter.DeviceItemCallback, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun newInstance() = DeviceFragment()
    }

    private lateinit var deviceList : RecyclerView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var disposable = Disposables.disposed()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_device, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        deviceList = view?.findViewById(R.id.device_list_view)!!
        deviceList.layoutManager = LinearLayoutManager(context)
        deviceList.animation = AnimationUtils.loadAnimation(context, R.anim.abc_slide_in_bottom)

        refreshLayout = view.findViewById(R.id.refresh)
        refreshLayout.setOnRefreshListener(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        disposable.dispose()
        disposable = DataManager.getDeviceList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    deviceList.adapter = DeviceAdapter(list).also {
                        it.setCallback(this)
                    }
                }
    }

    override fun onDetach() {
        super.onDetach()

        disposable.dispose()
    }

    override fun onClick(asset: DeviceAsset?) {
        fragmentManager.beginTransaction()
                .animateTransition()
                .addToBackStack("stack")
                .add(id, TaskFragment.newInstance(asset))
                .commit()
        Log.e("DeviceFragment", "Asset clicked on -> ${asset?.name}")
    }

    override fun onRefresh() {
        disposable.dispose()
        disposable = DataManager.getDeviceList()
                .timeout(15000, TimeUnit.MILLISECONDS, {
                    refreshLayout.isRefreshing = false
                    App.getMainHandler()?.post {
                        Toast.makeText(context, "Failed to fetch", Toast.LENGTH_SHORT).show()
                    }
                    it.onSubscribe(Disposables.disposed())
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    (deviceList.adapter as DeviceAdapter).setItems(list)
                    refreshLayout.isRefreshing = false
                }
    }

}
