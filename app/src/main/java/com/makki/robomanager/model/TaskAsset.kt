package com.makki.robomanager.model

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Max.Popovych on 09.06.18.
 */
class TaskAsset() : Parcelable{
    var id = 0
    var userId = 0
    var name = ""
    var task = ""
    var createdAt = ""
    var updatedAt = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        userId = parcel.readInt()
        name = parcel.readString()
        task = parcel.readString()
        createdAt = parcel.readString()
        updatedAt = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(userId)
        parcel.writeString(name)
        parcel.writeString(task)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskAsset> {
        override fun createFromParcel(parcel: Parcel): TaskAsset {
            return TaskAsset(parcel)
        }

        override fun newArray(size: Int): Array<TaskAsset?> {
            return arrayOfNulls(size)
        }
    }

}