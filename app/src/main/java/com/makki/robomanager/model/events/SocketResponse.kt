package com.makki.robomanager.model.events

import com.makki.robomanager.managers.utils.JsonParser

/**
 * @author Max.Popovych on 14.05.18.
 */
class SocketResponse(
        private var isMessage: Boolean) {

    var content = ""
    var throwable: Throwable? = null

    var type: SResponseType = SResponseType.UnknownMessage

    companion object {
        fun error() = SocketResponse(false).also {
            it.type = SResponseType.FailureMessage
        }
    }

    fun toAsset() = JsonParser.getStatusAsset(content)

    fun isMessage() = isMessage
}

enum class SResponseType {
    OpenMessage,
    StringMessage,
    ByteMessage,
    ClosingMessage,
    ClosedMessage,
    FailureMessage,
    UnknownMessage
}