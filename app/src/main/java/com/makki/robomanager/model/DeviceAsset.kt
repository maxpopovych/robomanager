package com.makki.robomanager.model

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Max.Popovych on 09.06.18.
 */
class DeviceAsset() : Parcelable {
    var id = 0
    var userId = 0
    var activeTaskId = 0
    var name = ""
    var createdAt = ""
    var updatedAt = ""

    fun isValid(): Boolean = !name.isBlank() && id > 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        userId = parcel.readInt()
        activeTaskId = parcel.readInt()
        name = parcel.readString()
        createdAt = parcel.readString()
        updatedAt = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(userId)
        parcel.writeInt(activeTaskId)
        parcel.writeString(name)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DeviceAsset> {
        override fun createFromParcel(parcel: Parcel): DeviceAsset {
            return DeviceAsset(parcel)
        }

        override fun newArray(size: Int): Array<DeviceAsset?> {
            return arrayOfNulls(size)
        }
    }
}